package it.polito.dp2.FDS.lab2.tests.ghioluca.test22;

import java.text.ParseException;
import java.util.*;

import it.polito.dp2.FDS.*;
import it.polito.dp2.FDS.tests.*;

public class SimpleFlightMonitor extends it.polito.dp2.FDS.tests.SimpleFlightMonitor {
	public SimpleFlightMonitor() throws FlightMonitorException {
		super();
	}

	@Override
	public void fill() throws FlightMonitorException {
		HashSet<String> seats = new HashSet<String>();
		seats.add("1A");
		Aircraft aircraft = new Aircraft("Boeing747", seats);
		addAircraft(aircraft);
		
		Aircraft aircraft2 = new Aircraft("Boeing748", seats);
		
		SimpleFlightReader flight = new SimpleFlightReader("XYZ", new Time(12, 30), "ABC", "AB123");
		addFlight(flight);
		
		SimpleFlightInstanceReader flightInstance;
		try {
			flightInstance = new SimpleFlightInstanceReader(aircraft2, it.polito.dp2.FDS.tests.Common.parseDate("2014-10-08 GMT+01:00"), 10, "Gate01", flight, FlightInstanceStatus.ARRIVED);
		} catch (ParseException e) {
			throw new FlightMonitorException(e);
		}
		flight.addFlightInstance(flightInstance);
		
		flightInstance.addPassenger(new SimplePassengerReader(true, flightInstance, "Pinco Pallino", "1A"));
	}
}