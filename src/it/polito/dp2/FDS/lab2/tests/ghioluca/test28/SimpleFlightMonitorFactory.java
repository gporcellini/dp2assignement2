package it.polito.dp2.FDS.lab2.tests.ghioluca.test28;

import it.polito.dp2.FDS.FlightMonitor;
import it.polito.dp2.FDS.FlightMonitorException;

public class SimpleFlightMonitorFactory extends it.polito.dp2.FDS.FlightMonitorFactory {
	@Override
	public FlightMonitor newFlightMonitor() throws FlightMonitorException {
		return new SimpleFlightMonitor();
	}
}
