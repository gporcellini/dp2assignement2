package it.polito.dp2.FDS.lab2.tests;

import it.polito.dp2.FDS.FlightMonitorException;
import it.polito.dp2.FDS.MalformedArgumentException;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;

import javax.xml.bind.JAXBException;

import org.xml.sax.SAXException;

public abstract class TestsAbstract extends it.polito.dp2.FDS.tests.TestsAbstract {
	private final static int assignmentNumber = 2;
	
	protected int getAssignmentNumber() {
		return assignmentNumber;
	}
	
	protected void runSerializerImpl(String[] args) throws FileNotFoundException, MalformedURLException, FlightMonitorException, MalformedArgumentException, JAXBException, SAXException {
		it.polito.dp2.FDS.sol2.FDSInfoSerializer.main(args);
	}
	
	protected String getParserClass() {
		return "it.polito.dp2.FDS.sol2.FlightMonitorFactory";
	}
	
	protected String getFlightInfoPropertyKey() {
		return "it.polito.dp2.FDS.sol2.FlightInfo.file";
	}
	
	protected File getSchemaFile() {
		return new File("./xsd/FDSInfo.xsd");
	}
}
