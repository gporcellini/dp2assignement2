package it.polito.dp2.FDS.tests;

import java.util.*;
import java.lang.UnsupportedOperationException;

import it.polito.dp2.FDS.*;

public abstract class SimpleFlightMonitor implements FlightMonitor {
	private HashSet<Aircraft> aircrafts = new HashSet<Aircraft>();
	private LinkedList<SimpleFlightReader> flights = new LinkedList<SimpleFlightReader>();
	
	public SimpleFlightMonitor() throws FlightMonitorException {
		fill();
	}
	
	@Override
	public Set<Aircraft> getAircrafts() {
		return aircrafts;
	}

	@Override
	public FlightReader getFlight(String number) throws MalformedArgumentException {
		if (number == null)
			throw new MalformedArgumentException();
		Iterator<SimpleFlightReader> i = flights.iterator();
		while (i.hasNext()) {
			SimpleFlightReader flight = i.next();
			if (flight.getNumber().equals(number))
				return flight;
		}
		return null;
	}

	@Override
	public FlightInstanceReader getFlightInstance(String number, GregorianCalendar date) throws MalformedArgumentException {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<FlightInstanceReader> getFlightInstances(String number, GregorianCalendar startDate, FlightInstanceStatus stat) throws MalformedArgumentException {
		if (startDate != null || stat != null)
			throw new UnsupportedOperationException();
		
		if (number != null) {
			SimpleFlightReader flight = (SimpleFlightReader)getFlight(number);
			if (flight == null)
				return new LinkedList<FlightInstanceReader>();
			return new LinkedList<FlightInstanceReader>(flight.getFlightInstances());
		}
		
		LinkedList<FlightInstanceReader> matchedFlightInstances = new LinkedList<FlightInstanceReader>();
		for (SimpleFlightReader flight : flights)
			matchedFlightInstances.addAll(flight.getFlightInstances());
		return matchedFlightInstances;
	}

	@Override
	public List<FlightReader> getFlights(String dep, String arr, Time startTime) throws MalformedArgumentException {
		if (dep != null || arr != null || startTime != null)
			throw new UnsupportedOperationException();
		return new LinkedList<FlightReader>(flights);
	}
	
	public void addAircraft(Aircraft aircraft) {
		aircrafts.add(aircraft);
	}
	
	public void addFlight(SimpleFlightReader flightReader) {
		flights.add(flightReader);
	}
	
	public abstract void fill() throws FlightMonitorException;
}
