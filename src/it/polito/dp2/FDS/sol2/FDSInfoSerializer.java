package it.polito.dp2.FDS.sol2;

import static javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI;

import java.io.File;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.xml.sax.SAXException;

import it.polito.dp2.FDS.Aircraft;
import it.polito.dp2.FDS.FlightInstanceReader;
import it.polito.dp2.FDS.FlightMonitor;
import it.polito.dp2.FDS.FlightMonitorException;
import it.polito.dp2.FDS.FlightMonitorFactory;
import it.polito.dp2.FDS.FlightReader;
import it.polito.dp2.FDS.MalformedArgumentException;
import it.polito.dp2.FDS.PassengerReader;
import it.polito.dp2.FDS.sol2.jaxb.SolFlightMonitor;
import it.polito.dp2.FDS.sol2.jaxb.StatusEnum;

public class FDSInfoSerializer {
	private SolFlightMonitor _jaxbMonitor;
	DateFormat dateFormat;
	it.polito.dp2.FDS.sol2.jaxb.ObjectFactory _jaxbFactory;

	/**
	 * @category Contructor
	 */
	public FDSInfoSerializer() throws FlightMonitorException {
		FlightMonitorFactory factory = FlightMonitorFactory.newInstance();
		FlightMonitor monitor = factory.newFlightMonitor();
		_jaxbFactory = new it.polito.dp2.FDS.sol2.jaxb.ObjectFactory();
		_jaxbMonitor = _jaxbFactory.createSolFlightMonitor();
		LoadAll(monitor);
		dateFormat = new SimpleDateFormat("dd/MM/yyyy z");
	}
	
	/**
	 * @category Constructor
	 * @param FlightMonitor to serialize in xml
	 * @throws FlightMonitorException 
	 */
	public FDSInfoSerializer(FlightMonitor monitor) throws FlightMonitorException {
		_jaxbFactory = new it.polito.dp2.FDS.sol2.jaxb.ObjectFactory();
		_jaxbMonitor = _jaxbFactory.createSolFlightMonitor();
		LoadAll(monitor);
		dateFormat = new SimpleDateFormat("dd/MM/yyyy z");
	}
	
	/**
	 * @category Loader
	 */
	private void LoadAll(FlightMonitor monitor) throws FlightMonitorException {
		try {
			LoadFlightInstances(LoadAicrafts(monitor), LoadFlights(monitor), monitor);
		} catch (DatatypeConfigurationException e) {
			//Should be never thrown
			e.printStackTrace();
			throw new FlightMonitorException("DatatypeConfigurationException @ JAXBFlightMonitor.LoadAll");
		}	
	}
	
	/**
	 * @category Loader
	 */
	private void LoadFlightInstances(Map<String, it.polito.dp2.FDS.sol2.jaxb.SolFlightMonitor.Aircraft> aicraftMap, 
			Map<String, it.polito.dp2.FDS.sol2.jaxb.SolFlightMonitor.Flight> flightMap, FlightMonitor monitor) throws DatatypeConfigurationException {
		try {
			for (FlightInstanceReader flightInstance : monitor.getFlightInstances(null, null, null)){
				SolFlightMonitor.FlightInstance jaxbFlightInstance = _jaxbFactory.createSolFlightMonitorFlightInstance();
				jaxbFlightInstance.setAircraftId(aicraftMap.get(flightInstance.getAircraft().model));
				jaxbFlightInstance.setFlightNumber(flightMap.get(flightInstance.getFlight().getNumber()));
				jaxbFlightInstance.setDelay(BigInteger.valueOf(flightInstance.getDelay()));
				jaxbFlightInstance.setGate(flightInstance.getDepartureGate());
				jaxbFlightInstance.setDeptDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(flightInstance.getDate()));
				jaxbFlightInstance.setStatus(StatusEnum.valueOf(flightInstance.getStatus().toString()));
				for(PassengerReader passenger : flightInstance.getPassengerReaders(null)){
					SolFlightMonitor.FlightInstance.Passenger jaxbPassenger = 
							_jaxbFactory.createSolFlightMonitorFlightInstancePassenger();
					jaxbPassenger.setName(passenger.getName());
					jaxbPassenger.setBoarded(passenger.boarded());
					jaxbPassenger.setSeat(passenger.getSeat());
					jaxbFlightInstance.getPassenger().add(jaxbPassenger);
				}
				_jaxbMonitor.getFlightInstance().add(jaxbFlightInstance);
			}
		} catch (MalformedArgumentException e) {
			// Never thrown because using null args
			e.printStackTrace();
		}
	}

	/**
	 * @category Loader
	 */
	private Map<String, SolFlightMonitor.Flight> LoadFlights(FlightMonitor monitor) throws DatatypeConfigurationException {
		try {
			Map<String, SolFlightMonitor.Flight> map = new HashMap<String, SolFlightMonitor.Flight>();
			for(FlightReader flight: monitor.getFlights(null, null, null)){
				SolFlightMonitor.Flight jaxbFlight = _jaxbFactory.createSolFlightMonitorFlight();
				jaxbFlight.setNumber(flight.getNumber());
				jaxbFlight.setDeptIATAcode(flight.getDepartureAirport());
				jaxbFlight.setDestIATAcode(flight.getDestinationAirport());
				XMLGregorianCalendar xmldTime = DatatypeFactory.newInstance().newXMLGregorianCalendar();
				xmldTime.clear();
				xmldTime.setHour(flight.getDepartureTime().getHour());
				xmldTime.setMinute(flight.getDepartureTime().getMinute());
				xmldTime.setSecond(0);
				jaxbFlight.setDeptTime(xmldTime);
				_jaxbMonitor.getFlight().add(jaxbFlight);
				map.put(flight.getNumber(), jaxbFlight);
			}
			
			return map;
		} catch (MalformedArgumentException e) {
			// never thrown when passing all null args
			e.printStackTrace();
			
			return null;
		}
	}

	/**
	 * @category Loader
	 */
	private Map<String, SolFlightMonitor.Aircraft> LoadAicrafts(FlightMonitor monitor) {
		Map<String, SolFlightMonitor.Aircraft> map = new HashMap<String, SolFlightMonitor.Aircraft>();
		for(Aircraft aircraft : monitor.getAircrafts()){
			SolFlightMonitor.Aircraft jaxbAircraft = _jaxbFactory.createSolFlightMonitorAircraft();
			jaxbAircraft.setModel(aircraft.model);
			jaxbAircraft.getSeat().addAll(aircraft.seats);
			_jaxbMonitor.getAircraft().add(jaxbAircraft);
			map.put(aircraft.model, jaxbAircraft);
		}
		
		return map;
	}
	
	/**
	 * @category Loader
	 */
	public void printAll(String fileName) throws FlightMonitorException {
		try {
			File file = new java.io.File(fileName);
			
			JAXBContext jaxbContext = JAXBContext.newInstance("it.polito.dp2.FDS.sol2.jaxb");
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			// output pretty printed
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			//reference schema in xml
			//jaxbMarshaller.setProperty(Marshaller.JAXB_NO_NAMESPACE_SCHEMA_LOCATION, Sol2Const.FDSINFO_XSD_SCHEMA);
			//validation schema
			SchemaFactory sf = SchemaFactory.newInstance(W3C_XML_SCHEMA_NS_URI);
            Schema schema = sf.newSchema(new File(Sol2Const.FDSINFO_XSD_SCHEMA_PATH));
            
            jaxbMarshaller.setSchema(schema);
            
			jaxbMarshaller.marshal(_jaxbMonitor, file);
		} catch (JAXBException e) {
			// Something went wrong marshaling
			throw new FlightMonitorException("Unmarshalling error:\n" + e.getCause());
		} catch (SAXException e) {
			// Validation Failed
			throw new FlightMonitorException("Unable to validate file or schema:1n" + e.getCause());
		}
	}

	/**
	 * @category main
	 * @param args
	 */
	public static void main(String[] args) {
		FDSInfoSerializer f;
		
		if(args.length != 1){
			System.out.println("Usage FDSInfoSerializer xmlfile");
		}
		try {
			f = new FDSInfoSerializer();
			f.printAll(args[0]);
		} catch (FlightMonitorException e) {
			System.err.println("Could not instantiate flight monitor object");
			e.printStackTrace();
			System.exit(1);
		}
	}
}
