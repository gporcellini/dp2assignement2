package it.polito.dp2.FDS.sol2;

import it.polito.dp2.FDS.FlightInstanceReader;
import it.polito.dp2.FDS.FlightMonitorException;
import it.polito.dp2.FDS.PassengerReader;
import it.polito.dp2.FDS.sol2.jaxb.SolFlightMonitor;

public class Sol2PassengerReader implements PassengerReader{
	private SolFlightMonitor.FlightInstance.Passenger _jaxbPassenger;
	private FlightInstanceReader _flightInstanceReader;
	
	/**
	 * @category Constructor
	 * @param jaxbPassenger
	 * @param flightInstanceReader
	 * @throws FlightMonitorException 
	 */
	public Sol2PassengerReader(SolFlightMonitor.FlightInstance.Passenger jaxbPassenger,
			FlightInstanceReader flightInstanceReader) throws FlightMonitorException {
		if(jaxbPassenger.isBoarded() && jaxbPassenger.getSeat() == null)
			throw new FlightMonitorException("Passenger " + jaxbPassenger.getName() + " is boarded but has no assigned seat");
		String seat = jaxbPassenger.getSeat();
		if(seat != null){
			if(flightInstanceReader.getAircraft() == null)
				throw new FlightMonitorException("No aircraft yet assigned to flightIstance, passenger"
						+ " couldn't have any assigned seat");
			if(!flightInstanceReader.getAircraft().seats.contains(seat)){
				throw new FlightMonitorException("FlightInstance(): no such seat for selected aircraft");
			}
		}
		_jaxbPassenger = jaxbPassenger;
		_flightInstanceReader = flightInstanceReader;
	}

	/**
	 * @category Getter
	 */
	@Override
	public boolean boarded() {
		return _jaxbPassenger.isBoarded();
	}

	/**
	 * @category Getter
	 */
	@Override
	public FlightInstanceReader getFlightInstance() {
		return _flightInstanceReader;
	}

	/**
	 * @category Getter
	 */
	@Override
	public String getName() {
		return _jaxbPassenger.getName();
	}

	/**
	 * @category Getter
	 */
	@Override
	public String getSeat() {
		return _jaxbPassenger.getSeat();
	}

}
