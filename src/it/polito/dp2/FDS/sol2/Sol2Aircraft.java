package it.polito.dp2.FDS.sol2;

import java.util.HashSet;
import java.util.Set;

import it.polito.dp2.FDS.Aircraft;
import it.polito.dp2.FDS.sol2.jaxb.SolFlightMonitor;

public class Sol2Aircraft extends Aircraft{

	/**
	 * @category Constructor
	 * @param model
	 * @param seats
	 */
	public Sol2Aircraft(String model, Set<String> seats) {
		super(model, seats);
	}
	
	/**
	 * @category Constructor
	 * @param jaxbAircraft
	 */
	public Sol2Aircraft(SolFlightMonitor.Aircraft jaxbAircraft){
		super(jaxbAircraft.getModel(), new HashSet<String>());
		this.seats.addAll(jaxbAircraft.getSeat());
	}
	
}
