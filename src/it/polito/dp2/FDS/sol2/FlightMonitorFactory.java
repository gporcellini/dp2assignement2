package it.polito.dp2.FDS.sol2;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.List;

import it.polito.dp2.FDS.FlightInstanceReader;
import it.polito.dp2.FDS.FlightMonitor;
import it.polito.dp2.FDS.FlightMonitorException;
import it.polito.dp2.FDS.FlightReader;
import it.polito.dp2.FDS.MalformedArgumentException;
import it.polito.dp2.FDS.PassengerReader;
import it.polito.dp2.FDS.Time;

public class FlightMonitorFactory extends it.polito.dp2.FDS.FlightMonitorFactory  {
	@Override
	public FlightMonitor newFlightMonitor() throws FlightMonitorException {
		Sol2FlightMonitor monitor = Sol2FlightMonitor.getInstance();
		
		monitor.parse(System.getProperty("it.polito.dp2.FDS.sol2.FlightInfo.file"));
		
		return monitor;
	}
	
	/**
	 * @category main
	 * @param args
	 */
	public static void main(String[] args){
		if(args.length != 2) {
			System.out.println("Missing Argument");
			return;
		}
		
		System.setProperty("it.polito.dp2.FDS.sol2.FlightInfo.file", args[0]);
		FlightMonitorFactory factory = new FlightMonitorFactory();
		try {
			FlightMonitor monitor = factory.newFlightMonitor();
			FDSInfoSerializer f = new FDSInfoSerializer(monitor);
			f.printAll(args[1]);
			
			//TEST getFlights
			List<FlightReader> flights =  monitor.getFlights(null, null, new Time(23, 10));
			for(FlightReader flightReader : flights){
				System.out.println(flightReader.getDepartureAirport() + " "
						+ flightReader.getDestinationAirport() + " "
						+ flightReader.getNumber() + " "
						+ flightReader.getDepartureTime().getHour() + ":"
						+ flightReader.getDepartureTime().getMinute());
			}
			
			List<FlightInstanceReader> flightInstances = monitor.getFlightInstances("ZX186", new GregorianCalendar(2010, 3-1, 7), null);
			for (FlightInstanceReader flightInstanceReader : flightInstances) {
				System.out.println(
						flightInstanceReader.getFlight().getNumber() + " " +
						new SimpleDateFormat("dd/MM/yyyyZ").format(flightInstanceReader.getDate().getTime()) + " " +
						flightInstanceReader.getStatus().toString()
						);
				for (PassengerReader passengerReader : flightInstanceReader.getPassengerReaders("a")){
					System.out.println(
							"     -"+passengerReader.getName());
				}
			}
		}catch (MalformedArgumentException e) {
			e.printStackTrace();
		} catch (FlightMonitorException e) {
			System.err.println("Could not instantiate flight monitor object");
			e.printStackTrace();
			System.exit(1);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
	}
}
