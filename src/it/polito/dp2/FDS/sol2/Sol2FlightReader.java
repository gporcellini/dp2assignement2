package it.polito.dp2.FDS.sol2;

import it.polito.dp2.FDS.FlightReader;
import it.polito.dp2.FDS.Time;
import it.polito.dp2.FDS.sol2.jaxb.SolFlightMonitor;

public class Sol2FlightReader implements FlightReader{
	private SolFlightMonitor.Flight _jaxbFlight;

	/**
	 * @category Constructor
	 * @param flight
	 */
	public Sol2FlightReader(SolFlightMonitor.Flight flight) {
		_jaxbFlight = flight;
	}
	
	/**
	 * @category Getter
	 */
	@Override
	public String getDepartureAirport() {
		return _jaxbFlight.getDeptIATAcode();
	}

	/**
	 * @category Getter
	 */
	@Override
	public Time getDepartureTime() {
		return new Time(_jaxbFlight.getDeptTime().getHour(), _jaxbFlight.getDeptTime().getMinute());
	}

	/**
	 * @category Getter
	 */
	@Override
	public String getDestinationAirport() {
		return _jaxbFlight.getDestIATAcode();
	}

	/**
	 * @category Getter
	 */
	@Override
	public String getNumber() {
		return _jaxbFlight.getNumber();
	}

}

