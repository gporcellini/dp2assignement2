package it.polito.dp2.FDS.sol2;

import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Set;

import it.polito.dp2.FDS.Aircraft;
import it.polito.dp2.FDS.FlightInstanceReader;
import it.polito.dp2.FDS.FlightInstanceStatus;
import it.polito.dp2.FDS.FlightMonitorException;
import it.polito.dp2.FDS.FlightReader;
import it.polito.dp2.FDS.MalformedArgumentException;
import it.polito.dp2.FDS.PassengerReader;
import it.polito.dp2.FDS.sol2.jaxb.SolFlightMonitor;

public class Sol2FlightInstanceReader implements FlightInstanceReader{
	private Aircraft _aircraft;
	private FlightReader _flightReader;
	private GregorianCalendar _deptDate;
	private int _delay;
	private String _gate;
	private Set<PassengerReader> _passengers;
	private FlightInstanceStatus _status;
	
	/**
	 * @category Constructor
	 * @param jaxbFlightInstance
	 * @param caller
	 * @throws FlightMonitorException
	 */
	public Sol2FlightInstanceReader(SolFlightMonitor.FlightInstance jaxbFlightInstance, Sol2FlightMonitor caller) throws FlightMonitorException {
		try {
			// NB names of this getters are misleading as they actually return an object reference
			_aircraft = caller.getAircraft(((SolFlightMonitor.Aircraft)jaxbFlightInstance.getAircraftId()).getModel());
			_flightReader = caller.getFlight(((SolFlightMonitor.Flight)jaxbFlightInstance.getFlightNumber()).getNumber());
			_delay = jaxbFlightInstance.getDelay().intValue();
			_gate = jaxbFlightInstance.getGate();
			_status = FlightInstanceStatus.valueOf(jaxbFlightInstance.getStatus().toString());
			_deptDate = jaxbFlightInstance.getDeptDate().toGregorianCalendar();
			_passengers = new HashSet<PassengerReader>();
			for(SolFlightMonitor.FlightInstance.Passenger jaxbPassenger : jaxbFlightInstance.getPassenger()){
				_passengers.add(new Sol2PassengerReader(jaxbPassenger, this));
			}
		} catch (MalformedArgumentException e) {
			//Should never happen as validation by xml xsd parser guarantees correct format
			throw new FlightMonitorException("Invalid Aircraft in flightInstance");
		}
	}
	
	/**
	 * @category Getter
	 */
	@Override
	public Aircraft getAircraft() {
		return _aircraft;
	}

	/**
	 * @category Getter
	 */
	@Override
	public GregorianCalendar getDate() {
		return _deptDate;
	}

	/**
	 * @category Getter
	 */
	@Override
	public int getDelay() {
		return _delay;
	}

	/**
	 * @category Getter
	 */
	@Override
	public String getDepartureGate() {
		return _gate;
	}

	/**
	 * @category Getter
	 */
	@Override
	public FlightReader getFlight() {
		return _flightReader;
	}

	/**
	 * @category Getter
	 */
	@Override
	public Set<PassengerReader> getPassengerReaders(String arg0) {//tested
		if(arg0 == null)
			return _passengers;
		Set<PassengerReader> subSet = new HashSet<PassengerReader>();
		for(PassengerReader passengerReader : _passengers){
			if(passengerReader.getName().toLowerCase().startsWith(arg0.toLowerCase()))
				subSet.add(passengerReader);
		}
		return subSet;
	}

	/**
	 * @category Getter
	 */
	@Override
	public FlightInstanceStatus getStatus() {
		return _status;
	}

}
