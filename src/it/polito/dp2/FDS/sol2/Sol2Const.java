package it.polito.dp2.FDS.sol2;

public final class Sol2Const {
	public static final String FDSINFO_XSD_SCHEMA_PATH = "xsd/FDSInfo.xsd";
	public static final String FDSINFO_XSD_SCHEMA = "FDSInfo.xsd";
}

